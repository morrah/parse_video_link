Parse Video Link version 1.1.0

Author: Helen Lazar (Morrah)
helen.lazar.77@gmail.com
https://bitbucket.org/morrah/

This is a simplest app allowing to parse links to videos hosted at the popular video hostings.
Provides REST-like architecture, so, you can extend main application class by your own actions to provide more functionality.
Has reach configuration abilities allowing to add more provided video hostings, map application actions to URI and toddle debug logging.
In version 1.1.0
provided video hostings
"youtube.com"
"vimeo.com"
Please remember that not all videos are available on vimeo.com without authentification, try it on /?link=https%3A%2F%2Fvimeo.com%2F170585872 request

actions included:
/index[?link=https%3A%2F%2Fyoutu.be%2FFSVFw9s2Aqw]
/info

INSTALLATION

Copy or rename distributional config file

cp config/config.tpl.php config/config.php
or
mv config/config.tpl.php config/config.php

Fill up your youtube and vimeo API keys

That's it!
