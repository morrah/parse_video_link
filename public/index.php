<?php

@include_once(__DIR__ . '/../vendor/autoload.php');
require_once(__DIR__ . '/../lib/AppClass.php');

$app = app\AppClass::getInstance();

$config = require_once(__DIR__ . '/../config/config.php');
$app->config = $config;
$app->run();
