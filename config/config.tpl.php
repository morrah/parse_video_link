<?php
return [
  'apis' => [
    'youtube.com' => [
      'api_key' => 'REPLACE_ME',
      'link_urls' => [
        'https://youtu.be/',
        'https://www.youtube.com/embed/',
      ],
      'link_id_param' => '',
      'api_url' => 'https://www.googleapis.com/youtube/v3/videos?part=snippet',
      'api_id_param' => '&id=',
      'api_key_param' => '&key=',
      'parse' => [
        'title' => function ($resp){return $resp['items']['0']['snippet']['title'];},
        'link' => function($resp){
          return 'https://www.youtube.com/embed/' . $resp['items']['0']['id'];
        },
        'embed' => function($resp){
          return '<iframe src="' . 'https://www.youtube.com/embed/' . $resp['items']['0']['id'] . '"></iframe>';},
      ],
      /* https://www.googleapis.com/youtube/v3/videos?part=snippet&id=FSVFw9s2Aqw&key=AIzaSyCaCH_4TCtPSKy-P34IJ2z3Eed0G0jime4
       https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id=FSVFw9s2Aqw&key=AIzaSyCaCH_4TCtPSKy-P34IJ2z3Eed0G0jime4
       take json_decode($resp)->items[0]['title' | 'description' | 'etag' ]
       embed https://youtu.be/FSVFw9s2Aqw
       <iframe width="420" height="315" src="https://www.youtube.com/embed/FSVFw9s2Aqw" frameborder="0" allowfullscreen></iframe>
      */
    ],
    'vimeo.com' => [
      'api_key' => 'REPLACE_ME',
      'link_urls' => [
        'https://vimeo.com/',
      ],
      'link_id_param' => '',
      'api_url' => 'https://api.vimeo.com/videos/',
      'api_id_param' => '',
      'api_key_param' => '?access_token=',
      'parse' => [
        'title' => function($resp){return $resp['name'];},
        'link' => function($resp){return $resp['link'];},
        'embed' => function($resp){return $resp['embed']['html'];},
      ],
      'client_id' => 'REPLACE_ME',
      'client_secret' => 'Mg+REPLACE_ME/8vhXvOqqDsi35LSZf2ngdtTW1OX56Lov/JJKDTOBGYHewhn6TsdHL1+pHeLDX1qAvHGtecYV+4C8dpevje3zS01lQ+OPLn',
      'auth_url' => 'https://api.vimeo.com/oauth/authorize',
      'access_token_url' => 'https://api.vimeo.com/oauth/access_token',
      'access_token' => 'REPLACE_ME',
      /* https://api.vimeo.com/videos/170585872?access_token=6bf8649624a68a514f8977b4690534de
      "name": "Alaska - Timelapse Film 4K",
      "description": "A year ago, ...",
      "link": "https://vimeo.com/170585872",
      "duration": 175,
      "width": 3840,
      "language": null,
      "height": 2160,
      "embed": {
          "html": "<iframe src=\"https://player.vimeo.com/video/170585872?badge=0&autopause=0&player_id=0\" width=\"3840\" height=\"2160\" frameborder=\"0\" title=\"Alaska - Timelapse Film 4K\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>"
      },
      "created_time": "2016-06-14T07:38:37+00:00",
      "modified_time": "2016-06-17T04:05:22+00:00",
      "release_time": "2016-06-14T07:38:37+00:00",
      */
    ],
  ],
  'action_map' => [
    // for example,
    /*'parse' => 'index',
    'ver' => 'info',*/
  ],
  'debug' => true,
];
