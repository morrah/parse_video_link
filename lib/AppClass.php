<?php
namespace app;

class AppClass{

  protected static $_instance;

  protected $config;

  protected $default;

  protected function __construct(){
    $this->default = null;
    $this->args = [];
    $this->version = '1.0.0';
  }

  public static function getInstance(){
    if(static::$_instance === null){
      static::$_instance = new static();
    }
    return static::$_instance;
  }

  public function run(){
    $this->debug_log('Application start!', __METHOD__);
    try{
      $this->handleRequest();
      if(method_exists($this, $this->action . 'Action'))
        call_user_func_array([$this, $this->action . 'Action'], $this->args);
      else
        $this->errorAction();
    }
    catch(Exception $e){
      $this->response = 'Ooops... It seems that an error occured...';
      error_log($e->getMessage());
      $this->send();
    }
  }

  public function indexAction(){
    $this->prepareResponseLink();
    $this->send();
  }

  public function infoAction(){
    $this->response = [
      "info" => [
        "version" => $this->version,
        "provided" => array_keys($this->config['apis']),
      ],
    ];
    $this->send();
  }

  public function errorAction(){
    $this->response = ['error' => 'Bad request!'];
    $this->send();
  }

  protected function handleRequest(){
    $req = [
      'host' => $_SERVER['HTTP_HOST'],
      'uri' => $_SERVER['REQUEST_URI'],
      'path' => explode('/', trim(substr($_SERVER['REQUEST_URI'],0,strpos($_SERVER['REQUEST_URI'],'?')), '/')),
      'query_params' => $this->parseQuery(),
    ];
    $this->request = $req;
    $this->action = !empty($this->request['path'][0]) ? $this->request['path'][0] : 'index';
    $this->map_action();
    $this->debug_log($this->request);
    $this->debug_log($this->action);
  }

  protected function prepareResponseLink(){
    $this->parseLink();
    $this->requestVS();
  }

  protected function send(){
    $resp = json_encode($this->response);
    header('content-type: application/json');
    echo($resp);
    exit();
  }

  protected function parseQuery(){
    if(!empty($_SERVER['QUERY_STRING'])){
      $raw = explode('&', $_SERVER['QUERY_STRING']);
      foreach($raw as $v){
        list($key, $val) = explode('=', $v);
        $ret[$key] = $val;
      }
      return array_merge($ret,(!empty($SERVER['POST']) ? $SERVER['POST'] : []));
    }
    return [];
  }

  protected function parseLink(){
    $this->debug_log("Start!", __METHOD__);
    $link = urldecode($this->request['query_params']['link']);
    $this->debug_log($link, __METHOD__);
    if(empty($link)){
      $this->response = ["message" => "Empty link", "link_example" => urlencode('https://youtu.be/FSVFw9s2Aqw')];
      $this->view =
      $this->send();
    }
    foreach($this->config['apis'] as $k => $v){
      foreach ($v['link_urls'] as $pattern) {
        $full_pattern = '/' . preg_quote($pattern, '/') . $v['link_id_param'] . '(.*)' . '/i';
        $this->debug_log($full_pattern, __METHOD__);
        // '/' . preg_quote($pattern, '/') . $v['link_id_param'] . '(.*)' . '/i'
        if(preg_match($full_pattern, $link, $matches)){
          $this->debug_log($matches, __METHOD__);
          $this->service = $k;
          $this->video_id = $matches[1];
          $this->api_url = $this->config['apis'][$k]['api_url'] . $this->config['apis'][$k]['api_id_param'] . $this->video_id . $this->config['apis'][$k]['api_key_param'] . $this->config['apis'][$k]['api_key'];
          $this->debug_log($this->api_url, __METHOD__);
          $broken = true;
          break;
        }
        if(isset($broken))
          break;
      }
    }
  }

  /**
   * App functionality core forms the request to neerer API
   * and parses response
   * curl is used only to speed up development, here we'd use a raw header generation
   */
  protected function requestVS(){
    $this->debug_log("Start!", __METHOD__);
    $curl = curl_init($this->api_url);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $this->json = curl_exec($curl);
    curl_close($curl);
    $this->api_response = $this->toArray(json_decode($this->json));
    $resp = [];
    foreach($this->config['apis'][$this->service]['parse'] as $k => $v){
      $resp[$k] = $v($this->api_response);
    }
    $this->response = $resp;
    $this->debug_log($this->response, __METHOD__);
  }

  protected function toArray($arg){
    $ret = is_object($arg)?(array)$arg:$arg;
    if(is_array($ret))
      foreach($ret as $k => $v){
        $ret[$k] = $this->toArray($v);
      }
    return $ret;
  }

  protected function debug_log($msg,$method = __METHOD__){
    if($this->config['debug']){
      ob_start();
      var_dump($msg);
      $msg = ob_get_contents();
      ob_end_clean();
      error_log($method . ': ' .$msg);
    }
  }

  protected function map_action(){
    if(!empty($this->config['action_map'][$this->action]))
      $this->action = $this->config['action_map'][$this->action];
  }

  private function __clone(){

  }

  private function __wakeup(){

  }

  public function __get($arg){
    return(isset($this->{$arg}) ? $this->{$arg} : $this->default);
  }

  public function __set($arg, $val){
    $this->{$arg} = $val;
  }

  public function __isset($arg){
    return isset($this->{$arg});
  }

  public function __unset($arg){
    if(isset($this->{$arg})){
      unset($this->{$arg});
    }
  }

}
